package com.parking.reservation.controller;

import com.parking.reservation.dto.StatusDto;
import com.parking.reservation.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@ShellComponent
public class ParkingController {

    @Autowired
    private ParkingService parkingService;

    @ShellMethod(value = "creating a parking lot")
    public String create_parking_lot(int slotCount) {
        return parkingService.createParkingLot(slotCount);
    }

    @ShellMethod(value = "parking")
    public String park(String registrationNumber, String color) {
        return parkingService.park(registrationNumber, color);
    }

    @ShellMethod(value = "leaving")
    public String leave(Integer slotNumber) {
        return parkingService.leave(slotNumber);
    }

    @ShellMethod(value = "leaving")
    public void status() {
        parkingService.status();
    }
    @ShellMethod(value = "getting registration numbers for cars with colour")
    public String registration_numbers_for_cars_with_colour(String color) {
        return parkingService.getRegistrationNumbersByColor(color);
    }

    @ShellMethod(value = "getting slot numbers for cars with registration number")
    public String slot_numbers_for_cars_with_registration_number(String registrationNumber) {
        return parkingService.getSlotNumberByRegistrationNumber(registrationNumber);
    }

    @ShellMethod(value = "getting slot numbers for cars with colour")
    public String slot_numbers_for_cars_with_colour(String color) {
        return parkingService.getSlotNumbersByColor(color);
    }

    @ShellMethod(value = "exiting")
    public void exit_prog() {
//        since exit was a keyword, I used exit_prog
        parkingService.exit();
    }
}
