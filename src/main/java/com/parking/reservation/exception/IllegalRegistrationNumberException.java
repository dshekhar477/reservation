package com.parking.reservation.exception;

public class IllegalRegistrationNumberException extends RuntimeException {

    public IllegalRegistrationNumberException() {}
    public IllegalRegistrationNumberException(String msg) {
        super(msg);
    }
}
