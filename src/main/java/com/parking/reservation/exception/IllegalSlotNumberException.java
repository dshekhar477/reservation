package com.parking.reservation.exception;

public class IllegalSlotNumberException extends RuntimeException{

    public IllegalSlotNumberException() {}
    public IllegalSlotNumberException(String msg) {
        super(msg);
    }
}
