package com.parking.reservation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = SlotNotAvailableException.class)
    public String slotNotAvailableException(SlotNotAvailableException exception) {
        return "Sorry, parking lot is full";
    }

    @ExceptionHandler(value = IllegalRegistrationNumberException.class)
    public String illegalRegistrationNumberException(IllegalRegistrationNumberException exception) {
        return "Registration number is incorrect";
    }

    @ExceptionHandler(value = IllegalSlotNumberException.class)
    public String illegalSlotNumberException(IllegalSlotNumberException exception) {
        return "This slot number does not exist";
    }

//    @ExceptionHandler(value = Exception.class)
//    public ResponseEntity<String> exception(Exception exception) {
//        return new ResponseEntity<>("Some error occurred", HttpStatus.OK);
//    }
}
