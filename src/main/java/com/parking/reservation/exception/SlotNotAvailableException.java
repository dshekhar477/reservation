package com.parking.reservation.exception;

public class SlotNotAvailableException extends RuntimeException{

    public SlotNotAvailableException() {}
    public SlotNotAvailableException(String msg) {
        super(msg);
    }
}
