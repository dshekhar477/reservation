package com.parking.reservation.dto;


public class CarDto {

    private Long id;
    private String registrationNumber;
    private String color;
    private SlotDto slotDto;
}
