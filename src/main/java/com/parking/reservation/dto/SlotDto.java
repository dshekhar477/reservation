package com.parking.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SlotDto {

    private Long id;
    private Integer slotNumber;
    private Boolean isAvailable;
    private CarDto carDto;
}
