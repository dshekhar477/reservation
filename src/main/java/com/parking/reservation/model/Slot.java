package com.parking.reservation.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "slot")
public class Slot {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private Integer slotNumber;

    @Column(nullable = false)
    private Boolean isAvailable;

    @OneToOne(mappedBy = "slot",cascade = CascadeType.ALL)
    private Car car;

    public Slot(Integer slotNumber) {
        this.slotNumber = slotNumber;
        this. isAvailable = true;
    }
}
