package com.parking.reservation.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique=true)
    private String registrationNumber;

    @Column(nullable = false)
    private String color;

    @OneToOne
    private Slot slot;

    public Car(String registrationNumber, String color, Slot slot) {
        this.registrationNumber = registrationNumber;
        this.color = color;
        this.slot = slot;
    }
}
