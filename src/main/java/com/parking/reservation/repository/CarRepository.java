package com.parking.reservation.repository;

import com.parking.reservation.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    @Query(value = "SELECT * FROM car WHERE registration_number= :registrationNumber", nativeQuery = true)
    Optional<Car> findByRegistrationNumber(@Param("registrationNumber") String registrationNumber);

    List<Car> findByColor(String color);

    @Modifying
    @Query(value = "DELETE FROM car WHERE id= :id", nativeQuery = true)
    void deleteById(@Param("id") Long id);

    @Query(value = "SELECT * FROM car WHERE color= :color", nativeQuery = true)
    List<Car> getRegistrationNumbersByColor(@Param("color") String color);
}
