package com.parking.reservation.repository;

import com.parking.reservation.model.Car;
import com.parking.reservation.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {

    @Query(value = "SELECT EXISTS(SELECT * FROM slot)", nativeQuery = true)
    Integer doesSlotsExist();

    @Query(value = "SELECT * FROM slot WHERE is_available=1 ORDER BY slot_number ASC LIMIT 1", nativeQuery = true)
    Optional<Slot> findNearestSlot();

    @Query(value = "SELECT * FROM slot WHERE slot_number= :slotNumber", nativeQuery = true)
    Optional<Slot> findBySlotNumber(@Param("slotNumber") Integer slotNumber);

    @Query(value = "SELECT * FROM slot", nativeQuery = true)
    List<Slot> getAllSlots();

    @Query(value = "SELECT * FROM slot", nativeQuery = true)
    List<Slot> getSlotNumberByRegistrationNumber();

    @Query(value = "SELECT s.slot_number FROM slot s JOIN car c ON s.id=c.slot_id WHERE c.registration_number = " +
                                                                        ":registrationNumber", nativeQuery = true)
    Optional<Integer> getSlotNumberByRegistrationNumber(@Param("registrationNumber") String registrationNumber);

    @Query(value = "SELECT s.* FROM slot s JOIN car c ON s.id=c.slot_id WHERE c.color = " +
            ":color", nativeQuery = true)
    List<Slot> getSlotNumbersByColor(@Param("color") String color);
}
