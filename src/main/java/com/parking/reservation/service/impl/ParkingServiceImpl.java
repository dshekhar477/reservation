package com.parking.reservation.service.impl;

import com.parking.reservation.dto.StatusDto;
import com.parking.reservation.exception.IllegalRegistrationNumberException;
import com.parking.reservation.exception.IllegalSlotNumberException;
import com.parking.reservation.exception.SlotNotAvailableException;
import com.parking.reservation.model.Car;
import com.parking.reservation.model.Slot;
import com.parking.reservation.repository.CarRepository;
import com.parking.reservation.repository.SlotRepository;
import com.parking.reservation.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private SlotRepository slotRepository;

    @Override
    public String createParkingLot(int slotCount) {
        if(slotRepository.doesSlotsExist()!=1) {
            List<Slot> slots = new ArrayList<>();
            for (int i = 1; i <= slotCount; i++) {
                Slot slot = new Slot(i);
                slots.add(slot);
            }
            slotRepository.saveAll(slots);
            return "Created a parking lot with " + slotCount + " slots";
        }
        return "Parking lot is already exists. Please exit and then try creating a new parking lot";
    }

    @Override
    public String park(String registrationNumber, String color) {
        if(carRepository.findByRegistrationNumber(registrationNumber).isPresent())
            throw new IllegalRegistrationNumberException("Car with this registration number already parked.");
//        The question says->The customer should be allocated a parking slot which is nearest to the entry. Now it depends
//        on whether the entry is near starting slots or ending slots. I am assuming the former.
        Optional<Slot> slotOptional = slotRepository.findNearestSlot();
        if(slotOptional.isEmpty())
            throw new SlotNotAvailableException("Sorry, parking lot is full");
        Slot slot = slotOptional.get();
        Car car = new Car(registrationNumber, color, slot);
        Car savedCar = carRepository.save(car);
        slot.setIsAvailable(false);
        slot.setCar(savedCar);
        slotRepository.save(slot);
        return "Allocated slot number: " + slot.getSlotNumber();
    }

    @Override
    public String leave(Integer slotNumber) {
        Optional<Slot> slotOptional = slotRepository.findBySlotNumber(slotNumber);
        if(slotOptional.isEmpty())
            throw new IllegalSlotNumberException();
        Slot slot = slotOptional.get();
        if(slot.getIsAvailable())
            throw new IllegalSlotNumberException("Slot is already vacant");
        carRepository.deleteById(slot.getCar().getId());
        slot.setIsAvailable(true);
        slot.setCar(null);
        slotRepository.save(slot);
        return "Slot number " + slotNumber + " is free";
    }

    @Override
    public void status() {
        List<Slot> slots = slotRepository.getAllSlots();
        System.out.println("Slot No. Registration No Colour");
        for (Slot slot : slots)
            if (!slot.getIsAvailable())
                System.out.println(slot.getSlotNumber() + " " + slot.getCar().getRegistrationNumber() + " " +
                                                                                    slot.getCar().getColor());
    }

    @Override
    public String getRegistrationNumbersByColor(String color) {
        String registrationNumberList = "";
        List<Car> cars = carRepository.getRegistrationNumbersByColor(color);
        if(cars.isEmpty())
            return "There are no cars with this color";
        for(Car car:cars)
            registrationNumberList += car.getRegistrationNumber() + ", ";

        return registrationNumberList.substring(0, registrationNumberList.length() - 2);
    }

    @Override
    public String getSlotNumberByRegistrationNumber(String registrationNumber) {
        Optional<Integer> slotNumberOptional = slotRepository.getSlotNumberByRegistrationNumber(registrationNumber);
        if(slotNumberOptional.isEmpty())
            return "This registration number car is not parked";
        return slotNumberOptional.get().toString();
    }

    @Override
    public String getSlotNumbersByColor(String color) {
        String slotNumberList = "";
        List<Slot> slots = slotRepository.getSlotNumbersByColor(color);
        if(slots.isEmpty())
            return "There are no cars with this color";
        for(Slot slot:slots)
            slotNumberList += slot.getSlotNumber() + ", ";

        return slotNumberList.substring(0, slotNumberList.length() - 2);
    }

    @Override
    public void exit() {
        carRepository.deleteAll();
        slotRepository.deleteAll();
    }
}
