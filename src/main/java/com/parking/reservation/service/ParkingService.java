package com.parking.reservation.service;

import com.parking.reservation.dto.StatusDto;

import java.util.List;

public interface ParkingService {
    String createParkingLot(int slotCount);

    String park(String registrationNumber, String color);

    String leave(Integer slotNumber);

    void status();

    String getRegistrationNumbersByColor(String color);

    String getSlotNumberByRegistrationNumber(String registrationNumber);

    String getSlotNumbersByColor(String color);

    void exit();
}
